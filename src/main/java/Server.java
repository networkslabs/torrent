import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ByteUtil;
import utils.Connection;
import utils.exception.ProtocolViolationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

public class Server implements Runnable {
    private static final int LOG_DELAY = 3000;
    private final static String UPLOAD_PATH = "uploads\\";
    private final Logger logger = LoggerFactory.getLogger(Server.class);
    private final ServerSocket serverSocket;

    public Server(int serverPort) throws IOException {
        new File(UPLOAD_PATH).mkdir();
        serverSocket = new ServerSocket(serverPort);
    }

    @Override
    public void run() {
        while (!serverSocket.isClosed()) {
            try {
                Socket socket = serverSocket.accept();
                logger.info("New client connected");
                new FileReceiver(socket).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private final class FileReceiver extends Thread {
        private final Connection connection;

        public FileReceiver(Socket socket) {
            this.connection = new Connection(socket);
        }

        private void receiveFile() throws IOException {
            byte[] data;
            try {
                data = connection.receive();
            } catch (IOException e) {
                throw e;
            }

            if (data.length < 1) {
                throw new ProtocolViolationException("File name lost");
            }

            String filename = new String(data, 0, data.length, StandardCharsets.UTF_8);
            try {
                data = connection.receive();
            } catch (IOException e) {
                throw e;
            }

            if (data.length != Long.BYTES) {
                throw new ProtocolViolationException("File size lost");
            }

            long filesize = ByteUtil.bytesToLong(data);

            File file = new File(UPLOAD_PATH + filename);

            FileOutputStream fout;
            try {
                fout = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw e;
            }

            long currentSize = 0L;

            long prevTime = System.currentTimeMillis();
            long summaryTime = 0;
            long currTime = 0;

            while (currentSize < filesize) {
                try {
                    data = connection.receive();
                } catch (IOException e) {
                    fout.close();
                    file.delete();
                    throw e;
                }

                int received = data.length;
                if (received == 0) {
                    fout.close();
                    file.delete();
                    throw new ProtocolViolationException("Data lost");
                }
                fout.write(data, 0, received);
                fout.flush();
                currentSize += received;

                currTime = System.currentTimeMillis() - prevTime;
                if (currTime > LOG_DELAY) {
                    prevTime = System.currentTimeMillis();
                    summaryTime += currTime;

                    logger.info(filename + ": " +
                            ((float) currentSize * 100) / filesize + "% "
                            + (float) (currentSize * 1000) / (1024 * summaryTime) + " kb/s ");
                }

            }

            currTime = System.currentTimeMillis() - prevTime;
            summaryTime += currTime;

            logger.info(filename + ": " +
                    ((float) currentSize * 100) / filesize + "% "
                    + (float) (currentSize * 1000) / (1024 * summaryTime) + " kb/s ");


            if (currentSize != filesize) {
                logger.error("Receiving failed");
                fout.close();
                file.delete();
            }

            fout.close();

            Duration duration = Duration.ofMillis(summaryTime);

            logger.info(filename + " received correctly in " + duration.toHoursPart() + ":" + duration.toMinutesPart() + ":" + duration.toSecondsPart());
        }

        @Override
        public void run() {
            try {
                receiveFile();
            } catch (IOException e) {
                logger.error("Receiving failed", e);

                try {
                    connection.send(new byte[]{1});
                    connection.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

                this.interrupt();
                return;
            }

            try {
                connection.send(new byte[]{0});
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            this.interrupt();
        }
    }
}
