package utils;

import utils.exception.ProtocolViolationException;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Connection implements Closeable {
    private final Socket socket;

    public Connection(Socket socket) {
        this.socket = socket;
    }

    public Connection(InetAddress address, int port) throws IOException {
        this(new Socket(address, port));
    }

    public void send(byte[] data) throws IOException {
        this.sendBytes(data, data.length);
    }

    public void sendBytes(byte[] data, int size) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(this.socket.getOutputStream());
        dataOutputStream.writeInt(size);
        dataOutputStream.write(data);
        dataOutputStream.flush();
    }

    public byte[] receive() throws IOException {
        DataInputStream dataInputStream = new DataInputStream(this.socket.getInputStream());
        int messageLength = dataInputStream.readInt();

        byte[] data = new byte[messageLength];

        try {
            dataInputStream.readFully(data, 0, messageLength);
        } catch (EOFException e) {
            throw new ProtocolViolationException("Data lost");
        } catch (IOException e) {
            throw e;
        }

        return data;
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }

    public int getPort() {
        return this.socket.getPort();
    }

    public boolean isClosed() {
        return this.socket.isClosed();
    }

    @Override
    public String toString() {
        return String.format("%s:%d", this.getInetAddress(), this.getPort());
    }

    @Override
    public void close() throws IOException {
        this.socket.close();
    }
}
