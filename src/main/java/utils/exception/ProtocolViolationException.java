package utils.exception;

import java.io.IOException;

public class ProtocolViolationException extends IOException {

    public ProtocolViolationException() {
    }

    public ProtocolViolationException(String message) {
        super(message);
    }

    public ProtocolViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
