import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ByteUtil;
import utils.Connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

public class Client implements Runnable {
    private final static int KILO_BYTE = 1024;
    private final static int BUFFER_SIZE = 100 * KILO_BYTE;
    private final Connection connection;
    private final File file;
    private final FileInputStream fin;
    private final Logger logger = LoggerFactory.getLogger(Client.class);

    public Client(String serverIp, int port, String filePath) throws IOException {
        this.file = new File(filePath);

        if (file.isDirectory()) {
            logger.error(file.getCanonicalPath() + " is directory");
            throw new IllegalArgumentException("File is directory");
        }

        this.fin = new FileInputStream(file);

        try {
            logger.info("Connecting to " + serverIp + ":" + port);
            this.connection = new Connection(InetAddress.getByName(serverIp), port);
        } catch (IOException e) {
            logger.error("Connection failed", e);
            throw e;
        }
    }

    @Override
    public void run() {
        try {
            sendFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendFile() throws IOException {
        logger.info("Sending file " + file.getCanonicalPath());

        long size = file.length();

        String fileName = file.getName();
        try {
            connection.send(fileName.getBytes(StandardCharsets.UTF_8));
            connection.send(ByteUtil.longToBytes(size));
        } catch (IOException e) {
            byte[] code = connection.receive();
            logger.error("Sending file failed with code: " + code[0], e);
            connection.close();
            return;
        }

        long sent = 0L;

        while (sent != size) {
            byte[] data = new byte[BUFFER_SIZE];
            int read;
            try {
                read = fin.read(data, 0, BUFFER_SIZE);
            } catch (IOException e) {
                logger.error("Sending file failed", e);
                connection.close();
                return;
            }
            try {
                connection.sendBytes(data, read);
            } catch (IOException e) {
                byte[] code = connection.receive();
                logger.error("Sending file failed with code: " + code[0], e);
                connection.close();
                return;
            }
            sent += read;
        }

        fin.close();


        byte[] code = connection.receive();

        if (code.length == 1 && code[0] == 0) {
            logger.info("File sent correctly");
        } else {
            logger.error("Sending file failed");
        }

        connection.close();
    }
}
