import java.io.IOException;

public class ClientApp {
    public static void main(String[] args) throws IOException {

        if (args.length != 3) {
            System.err.println("Usage: <serverIp> <serverPort> <file>");
            return;
        }

        new Client(args[0], Integer.parseInt(args[1], 10), args[2]).run();
    }
}
