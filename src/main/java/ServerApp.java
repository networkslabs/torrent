import java.io.IOException;

public class ServerApp {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: <serverPort>");
            return;
        }

        new Server(Integer.parseInt(args[0], 10)).run();
    }
}
